import os
import time

from fastapi import APIRouter, File, Form
from starlette.requests import Request
from starlette.responses import Response

from util.utils import check_duplicated_path
from config import DATA_PATH
from aiofiles import open as async_open

router = APIRouter()


@router.post("/data/", status_code=200)
async def persist_data(data: bytes = File(...), filename: str = Form(...)):
    if filename is None:
        filename = str(time.time())
    directory = DATA_PATH
    final_path = os.path.join(directory, filename)
    final_path = check_duplicated_path(final_path)
    with open(final_path, 'wb') as f:
        f.write(data)
    return {'file_size': len(data), 'filename': os.path.basename(final_path)}


@router.post("/send/", status_code=200)
async def receive_data(request: Request, filename: str = None, dir_name: str = ""):
    print(f"filename: {filename}")
    print(f"dirname: {dir_name}")
    if filename is None:
        filename = str(time.time())
    root = DATA_PATH
    dir_path = os.path.join(root, dir_name)
    # if dir_name != "" and dir_name != ".":
    #     dir_path = check_duplicated_path(dir_path)
    os.makedirs(dir_path, exist_ok=True)
    final_path = os.path.join(dir_path, filename)
    final_path = check_duplicated_path(final_path)
    total_len = 0
    async with async_open(final_path, 'wb') as buffer:
        async for chunk in request.stream():
            total_len += len(chunk)
            await buffer.write(chunk)

    return {'file_size': total_len, 'filename': os.path.basename(final_path)}
