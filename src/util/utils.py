import os

def check_duplicated_path(src_path):
    if os.path.exists(src_path):
        directory = os.path.dirname(src_path)
        filename = os.path.basename(src_path)
        components = filename.split(".")
        if len(components) > 1:
            ext = ".".join(components[1:])
            filename = filename.replace(f".{ext}", f"(dup).{ext}")
        else:
            filename = filename + "(dup)"

        return check_duplicated_path(os.path.join(directory, filename))
    else:
        return src_path
