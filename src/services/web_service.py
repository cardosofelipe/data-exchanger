from fastapi import FastAPI

from routers import data


# from persistence.database import engine as global_engine


def create_app():
    app = FastAPI()
    app.include_router(data.router)

    @app.get("/")
    def read_root():
        return {"message": "Welcome to Data Exchanger"}

    return app
