# Data Exchanger client
A simple python client for sending files to a Data Exchanger instance

## Setup
Just install the requirements in you virtual environment by launching (from the root of the client):

`pip install -r requirements.txt`
## Usage
Once you have the requirements installed, you can launch the main.py file with the proper arguments, 
with the argument -h you can see the following help:

````

usage: main.py [-h] [-d DIR] [-f FILE] [-c CUSTOM]

Data Exchanger client

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --dir DIR     Specify dir path
  -f FILE, --file FILE  Specify file path
  -c CUSTOM, --custom CUSTOM
                        Specify custom dir name to create in destination server
````

It is mandatory to specify a file path or a dir path, custom directory is optional.

### Example:
You want to send all the data contained in the folder 'twitter', this folder has the following structure:
````
twitter/
    jan/
        data00.csv
        data01.csv
    feb/
        data00.csv
    mar/
        data00.csv
        data01.csv
        data02.csv
````
The 'twitter' folder absolute path is: '/home/user/twitter'.

Just launch the following command (from the root of the client):

`python main.py -d /home/user/twitter`

to upload the twitter folder and all its content to the Data Exchanger instance, on the remote server the same folder structure will appear.

If you want to upload all the files to a remote custom folder (e.g., 'mydata'):

`python main.py -d /home/user/twitter -c mydata`
