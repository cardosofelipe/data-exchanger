import argparse
import glob
import os

from uploader import upload_file


# upload_file("/home/felipe/Downloads/zoom_amd64.deb")
# upload_file("main.py")

def setup_argparser():
    arg_parse = argparse.ArgumentParser(description='Data Exchanger client')
    arg_parse.add_argument(
        "-d", "--dir", help="Specify dir path",
        default=None,
        type=str,
        required=False
    )

    arg_parse.add_argument(
        "-f", "--file", help="Specify file path",
        default=None,
        type=str,
        required=False
    )

    arg_parse.add_argument(
        "-c", "--custom", help="Specify custom dir name to create in destination server",
        default=None,
        type=str,
        required=False
    )

    return arg_parse


argument_parser = setup_argparser()


def send_file(f, root_dir=None, custom_dir=None):
    if os.path.isfile(f):
        upload_file(f, root_dir=root_dir, custom_dir=custom_dir)


def send_directory(root_path, custom_dir=None):
    files = glob.glob(f"{root_path}/**/*", recursive=True)
    # print(f"Found files: {files}")
    for f in files:
        send_file(f, root_dir=root_path, custom_dir=custom_dir)


if __name__ == '__main__':
    args = argument_parser.parse_args()
    directory_path = args.dir
    file_path = args.file
    custom = args.custom

    if directory_path is None and file_path is None:
        print(
            "You should specify one directory or one file path! Launch the program with -h argument for help")

    if directory_path is not None:
        send_directory(directory_path, custom_dir=custom)
    elif file_path is not None:
        send_file(file_path, custom_dir=custom)
