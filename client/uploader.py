import os
from pathlib import Path

import requests
from tqdm import tqdm

SERVER_URL = os.getenv("SERVER_URL", "http://localhost:3000")


def read_in_chunks(file_object, chunk_size=65536, pbar=None):
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        if pbar:
            pbar.update()
        yield data


def upload_file(filepath, root_dir=None, custom_dir=None):
    path = Path(filepath)
    path_dir = path.parent
    # print(path_dir)
    total_size = path.stat().st_size
    filename = path.name
    chunk_size = 65536
    if root_dir is None:
        dir_name = path_dir.name
    else:
        path_parts = str(path_dir).split(f"{root_dir}/")
        if len(path_parts) > 1:
            # takes all parts except the first one, supports also repeated dir patterns
            last_part = os.path.join(*path_parts[1:])
        else:
            last_part = ""
        dir_name = os.path.join(f"{Path(root_dir).name}/", last_part)
    if custom_dir is not None:
        dir_name = os.path.join(custom_dir, dir_name)
    upload_url = f"{SERVER_URL}/send/?filename={filename}&dir_name={dir_name}"

    f = open(filepath, 'rb')

    chunk_num = 0
    with tqdm(desc=filepath, total=int(total_size / chunk_size)) as pbar:
        try:
            requests.post(upload_url, data=read_in_chunks(f, pbar=pbar))
        except Exception as e:
            print(f"There was an error while uploading chunk {chunk_num} of file {filepath}: ", e)
