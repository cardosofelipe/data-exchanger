FROM python:3.8-slim-buster

LABEL maintainer="Felipe Cardoso "
LABEL email="felipe.cardoso@supsi.ch"
LABEL version=0.1
COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY src /app
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH "${PYTONPATH}:/app"
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]