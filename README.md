# Data Exchanger
[![pipeline status](https://gitlab.com/cardosofelipe/data-exchanger/badges/master/pipeline.svg)](https://gitlab.com/cardosofelipe/data-exchanger/-/commits/master)


## Usage
Just use the /send/ endpoint with a binary payload in order to send the data. You can use the _filename_
URL parameter to send the filename.

### Example by using the server on http://myserver.com
#### curl

`curl http://myserver.com/send/?filename=myfile.png --data-binary "@myfile.png" --progress-bar | tee /dev/null`
